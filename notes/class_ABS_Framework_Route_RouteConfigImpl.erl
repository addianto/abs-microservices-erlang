-module(class_ABS_Framework_Route_RouteConfigImpl).
-include_lib("../include/abs_types.hrl").
-export([m_route/1, get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"RouteConfig">>, <<"Object">> ].

exported() -> #{ <<"route">> => { 'm_route', <<"ABS.StdLib.String">>, [ { <<"url">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }] } }.

%% --- Internal state and low-level accessors

-record(state,{'class'=class_ABS_Framework_Route_RouteConfigImpl}).
'init_internal'()->
    #state{}.

'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

'set_val_internal'(S,S,S)->
    throw(badarg).
'get_state_for_modelapi'(S)->
    [
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% framework/Route.abs:12
 %% framework/Route.abs:12
m_route(V_url_0)->
    case V_url_0 of
                <<"/"/utf8>>-><<"MStoryBoardResource.StoryBoardResourceImpl@list"/utf8>>;
                <<"/api/expense/list.abs"/utf8>>-><<"MExpenseResource.ExpenseResourceImpl@list"/utf8>>;
                <<"/api/expense/detail.abs"/utf8>>-><<"MExpenseResource.ExpenseResourceImpl@detail"/utf8>>;
                <<"/api/expense/create.abs"/utf8>>-><<"MExpenseResource.ExpenseResourceImpl@create"/utf8>>;
                <<"/api/expense/save.abs"/utf8>>-><<"MExpenseResource.ExpenseResourceImpl@save"/utf8>>;
                <<"/api/expense/edit.abs"/utf8>>-><<"MExpenseResource.ExpenseResourceImpl@edit"/utf8>>;
                <<"/api/expense/update.abs"/utf8>>-><<"MExpenseResource.ExpenseResourceImpl@update"/utf8>>;
                <<"/api/expense/delete.abs"/utf8>>-><<"MExpenseResource.ExpenseResourceImpl@delete"/utf8>>;
                <<"/api/income/list.abs"/utf8>>-><<"MIncomeResource.IncomeResourceImpl@list"/utf8>>;
                <<"/api/income/detail.abs"/utf8>>-><<"MIncomeResource.IncomeResourceImpl@detail"/utf8>>;
                <<"/api/income/create.abs"/utf8>>-><<"MIncomeResource.IncomeResourceImpl@create"/utf8>>;
                <<"/api/income/save.abs"/utf8>>-><<"MIncomeResource.IncomeResourceImpl@save"/utf8>>;
                <<"/api/income/edit.abs"/utf8>>-><<"MIncomeResource.IncomeResourceImpl@edit"/utf8>>;
                <<"/api/income/update.abs"/utf8>>-><<"MIncomeResource.IncomeResourceImpl@update"/utf8>>;
                <<"/api/income/delete.abs"/utf8>>-><<"MIncomeResource.IncomeResourceImpl@delete"/utf8>>;
                <<"/api/summary/list.abs"/utf8>>-><<"MSummaryResource.SummaryResourceImpl@list"/utf8>>;
                <<"/api/summary/detail.abs"/utf8>>-><<"MSummaryResource.SummaryResourceImpl@detail"/utf8>>;
                <<"/api/summary/create.abs"/utf8>>-><<"MSummaryResource.SummaryResourceImpl@create"/utf8>>;
                <<"/api/summary/save.abs"/utf8>>-><<"MSummaryResource.SummaryResourceImpl@save"/utf8>>;
                <<"/api/summary/edit.abs"/utf8>>-><<"MSummaryResource.SummaryResourceImpl@edit"/utf8>>;
                <<"/api/summary/update.abs"/utf8>>-><<"MSummaryResource.SummaryResourceImpl@update"/utf8>>;
                <<"/api/summary/delete.abs"/utf8>>-><<"MSummaryResource.SummaryResourceImpl@delete"/utf8>>;
                <<"/api/donor/list.abs"/utf8>>-><<"MDonorResource.DonorResourceImpl@list"/utf8>>;
                <<"/api/donor/detail.abs"/utf8>>-><<"MDonorResource.DonorResourceImpl@detail"/utf8>>;
                <<"/api/donor/create.abs"/utf8>>-><<"MDonorResource.DonorResourceImpl@create"/utf8>>;
                <<"/api/donor/save.abs"/utf8>>-><<"MDonorResource.DonorResourceImpl@save"/utf8>>;
                <<"/api/donor/edit.abs"/utf8>>-><<"MDonorResource.DonorResourceImpl@edit"/utf8>>;
                <<"/api/donor/update.abs"/utf8>>-><<"MDonorResource.DonorResourceImpl@update"/utf8>>;
                <<"/api/donor/delete.abs"/utf8>>-><<"MDonorResource.DonorResourceImpl@delete"/utf8>>;
                <<"/api/story-board/list.abs"/utf8>>-><<"MStoryBoardResource.StoryBoardResourceImpl@list"/utf8>>;
                <<"/api/story-board/detail.abs"/utf8>>-><<"MStoryBoardResource.StoryBoardResourceImpl@detail"/utf8>>;
                <<"/api/story-board/create.abs"/utf8>>-><<"MStoryBoardResource.StoryBoardResourceImpl@create"/utf8>>;
                <<"/api/story-board/save.abs"/utf8>>-><<"MStoryBoardResource.StoryBoardResourceImpl@save"/utf8>>;
                <<"/api/story-board/edit.abs"/utf8>>-><<"MStoryBoardResource.StoryBoardResourceImpl@edit"/utf8>>;
                <<"/api/story-board/update.abs"/utf8>>-><<"MStoryBoardResource.StoryBoardResourceImpl@update"/utf8>>;
                <<"/api/story-board/delete.abs"/utf8>>-><<"MStoryBoardResource.StoryBoardResourceImpl@delete"/utf8>>;
                <<"/api/automatic-report/list.abs"/utf8>>-><<"MAutomaticReportResource.AutomaticReportResourceImpl@list"/utf8>>;
                <<"/api/member-notification/list.abs"/utf8>>-><<"MMemberNotificationResource.MemberNotificationResourceImpl@list"/utf8>>;
                <<"/api/member-notification/detail.abs"/utf8>>-><<"MMemberNotificationResource.MemberNotificationResourceImpl@detail"/utf8>>;
                <<"/api/member-notification/create.abs"/utf8>>-><<"MMemberNotificationResource.MemberNotificationResourceImpl@create"/utf8>>;
                <<"/api/member-notification/save.abs"/utf8>>-><<"MMemberNotificationResource.MemberNotificationResourceImpl@save"/utf8>>;
                <<"/api/member-notification/edit.abs"/utf8>>-><<"MMemberNotificationResource.MemberNotificationResourceImpl@edit"/utf8>>;
                <<"/api/member-notification/update.abs"/utf8>>-><<"MMemberNotificationResource.MemberNotificationResourceImpl@update"/utf8>>;
                <<"/api/member-notification/delete.abs"/utf8>>-><<"MMemberNotificationResource.MemberNotificationResourceImpl@delete"/utf8>>;
                <<"/api/item-donation/list.abs"/utf8>>-><<"MItemDonationResource.ItemDonationResourceImpl@list"/utf8>>;
                <<"/api/item-donation/detail.abs"/utf8>>-><<"MItemDonationResource.ItemDonationResourceImpl@detail"/utf8>>;
                <<"/api/item-donation/create.abs"/utf8>>-><<"MItemDonationResource.ItemDonationResourceImpl@create"/utf8>>;
                <<"/api/item-donation/save.abs"/utf8>>-><<"MItemDonationResource.ItemDonationResourceImpl@save"/utf8>>;
                <<"/api/item-donation/edit.abs"/utf8>>-><<"MItemDonationResource.ItemDonationResourceImpl@edit"/utf8>>;
                <<"/api/item-donation/update.abs"/utf8>>-><<"MItemDonationResource.ItemDonationResourceImpl@update"/utf8>>;
                <<"/api/item-donation/delete.abs"/utf8>>-><<"MItemDonationResource.ItemDonationResourceImpl@delete"/utf8>>;
                <<"/api/money-donation/list.abs"/utf8>>-><<"MMoneyDonationResource.MoneyDonationResourceImpl@list"/utf8>>;
                <<"/api/money-donation/detail.abs"/utf8>>-><<"MMoneyDonationResource.MoneyDonationResourceImpl@detail"/utf8>>;
                <<"/api/money-donation/create.abs"/utf8>>-><<"MMoneyDonationResource.MoneyDonationResourceImpl@create"/utf8>>;
                <<"/api/money-donation/save.abs"/utf8>>-><<"MMoneyDonationResource.MoneyDonationResourceImpl@save"/utf8>>;
                <<"/api/money-donation/edit.abs"/utf8>>-><<"MMoneyDonationResource.MoneyDonationResourceImpl@edit"/utf8>>;
                <<"/api/money-donation/update.abs"/utf8>>-><<"MMoneyDonationResource.MoneyDonationResourceImpl@update"/utf8>>;
                <<"/api/money-donation/delete.abs"/utf8>>-><<"MMoneyDonationResource.MoneyDonationResourceImpl@delete"/utf8>>;
                <<"/api/confirmation/list.abs"/utf8>>-><<"MConfirmationResource.ConfirmationResourceImpl@list"/utf8>>;
                <<"/api/confirmation/detail.abs"/utf8>>-><<"MConfirmationResource.ConfirmationResourceImpl@detail"/utf8>>;
                <<"/api/confirmation/create.abs"/utf8>>-><<"administrator,staff,donor:MConfirmationResource.ConfirmationResourceImpl@create"/utf8>>;
                <<"/api/confirmation/save.abs"/utf8>>-><<"administrator,staff,donor:MConfirmationResource.ConfirmationResourceImpl@save"/utf8>>;
                <<"/api/confirmation/edit.abs"/utf8>>-><<"administrator,staff,donor:MConfirmationResource.ConfirmationResourceImpl@edit"/utf8>>;
                <<"/api/confirmation/update.abs"/utf8>>-><<"administrator,staff,donor:MConfirmationResource.ConfirmationResourceImpl@update"/utf8>>;
                <<"/api/confirmation/delete.abs"/utf8>>-><<"administrator,staff,donor:MConfirmationResource.ConfirmationResourceImpl@delete"/utf8>>;
                <<"/api/program/list.abs"/utf8>>-><<"MProgramResource.ProgramResourceImpl@list"/utf8>>;
                <<"/api/program/detail.abs"/utf8>>-><<"MProgramResource.ProgramResourceImpl@detail"/utf8>>;
                <<"/api/program/create.abs"/utf8>>-><<"MProgramResource.ProgramResourceImpl@create"/utf8>>;
                <<"/api/program/save.abs"/utf8>>-><<"MProgramResource.ProgramResourceImpl@save"/utf8>>;
                <<"/api/program/edit.abs"/utf8>>-><<"MProgramResource.ProgramResourceImpl@edit"/utf8>>;
                <<"/api/program/update.abs"/utf8>>-><<"MProgramResource.ProgramResourceImpl@update"/utf8>>;
                <<"/api/program/delete.abs"/utf8>>-><<"MProgramResource.ProgramResourceImpl@delete"/utf8>>;
                <<"/api/continuous-program/list.abs"/utf8>>-><<"MContinuousProgramResource.ContinuousProgramResourceImpl@list"/utf8>>;
                <<"/api/continuous-program/detail.abs"/utf8>>-><<"MContinuousProgramResource.ContinuousProgramResourceImpl@detail"/utf8>>;
                <<"/api/continuous-program/create.abs"/utf8>>-><<"MContinuousProgramResource.ContinuousProgramResourceImpl@create"/utf8>>;
                <<"/api/continuous-program/save.abs"/utf8>>-><<"MContinuousProgramResource.ContinuousProgramResourceImpl@save"/utf8>>;
                <<"/api/continuous-program/edit.abs"/utf8>>-><<"MContinuousProgramResource.ContinuousProgramResourceImpl@edit"/utf8>>;
                <<"/api/continuous-program/update.abs"/utf8>>-><<"MContinuousProgramResource.ContinuousProgramResourceImpl@update"/utf8>>;
                <<"/api/continuous-program/delete.abs"/utf8>>-><<"MContinuousProgramResource.ContinuousProgramResourceImpl@delete"/utf8>>;
                <<"/api/event/list.abs"/utf8>>-><<"MEventResource.EventResourceImpl@list"/utf8>>;
                <<"/api/event/detail.abs"/utf8>>-><<"MEventResource.EventResourceImpl@detail"/utf8>>;
                <<"/api/event/create.abs"/utf8>>-><<"MEventResource.EventResourceImpl@create"/utf8>>;
                <<"/api/event/save.abs"/utf8>>-><<"MEventResource.EventResourceImpl@save"/utf8>>;
                <<"/api/event/edit.abs"/utf8>>-><<"MEventResource.EventResourceImpl@edit"/utf8>>;
                <<"/api/event/update.abs"/utf8>>-><<"MEventResource.EventResourceImpl@update"/utf8>>;
                <<"/api/event/delete.abs"/utf8>>-><<"MEventResource.EventResourceImpl@delete"/utf8>>;
                <<"/api/institutional-beneficiary/list.abs"/utf8>>-><<"MInstitutionalBeneficiaryResource.InstitutionalBeneficiaryResourceImpl@list"/utf8>>;
                <<"/api/institutional-beneficiary/detail.abs"/utf8>>-><<"MInstitutionalBeneficiaryResource.InstitutionalBeneficiaryResourceImpl@detail"/utf8>>;
                <<"/api/institutional-beneficiary/create.abs"/utf8>>-><<"MInstitutionalBeneficiaryResource.InstitutionalBeneficiaryResourceImpl@create"/utf8>>;
                <<"/api/institutional-beneficiary/save.abs"/utf8>>-><<"MInstitutionalBeneficiaryResource.InstitutionalBeneficiaryResourceImpl@save"/utf8>>;
                <<"/api/institutional-beneficiary/edit.abs"/utf8>>-><<"MInstitutionalBeneficiaryResource.InstitutionalBeneficiaryResourceImpl@edit"/utf8>>;
                <<"/api/institutional-beneficiary/update.abs"/utf8>>-><<"MInstitutionalBeneficiaryResource.InstitutionalBeneficiaryResourceImpl@update"/utf8>>;
                <<"/api/institutional-beneficiary/delete.abs"/utf8>>-><<"MInstitutionalBeneficiaryResource.InstitutionalBeneficiaryResourceImpl@delete"/utf8>>;
                <<"/api/individual-beneficiary/list.abs"/utf8>>-><<"MIndividualBeneficiaryResource.IndividualBeneficiaryResourceImpl@list"/utf8>>;
                <<"/api/individual-beneficiary/detail.abs"/utf8>>-><<"MIndividualBeneficiaryResource.IndividualBeneficiaryResourceImpl@detail"/utf8>>;
                <<"/api/individual-beneficiary/create.abs"/utf8>>-><<"MIndividualBeneficiaryResource.IndividualBeneficiaryResourceImpl@create"/utf8>>;
                <<"/api/individual-beneficiary/save.abs"/utf8>>-><<"MIndividualBeneficiaryResource.IndividualBeneficiaryResourceImpl@save"/utf8>>;
                <<"/api/individual-beneficiary/edit.abs"/utf8>>-><<"MIndividualBeneficiaryResource.IndividualBeneficiaryResourceImpl@edit"/utf8>>;
                <<"/api/individual-beneficiary/update.abs"/utf8>>-><<"MIndividualBeneficiaryResource.IndividualBeneficiaryResourceImpl@update"/utf8>>;
                <<"/api/individual-beneficiary/delete.abs"/utf8>>-><<"MIndividualBeneficiaryResource.IndividualBeneficiaryResourceImpl@delete"/utf8>>;
                <<"/api/product/list.abs"/utf8>>-><<"MProductResource.ProductResourceImpl@list"/utf8>>;
                <<"/api/product/detail.abs"/utf8>>-><<"MProductResource.ProductResourceImpl@detail"/utf8>>;
                <<"/api/product/create.abs"/utf8>>-><<"MProductResource.ProductResourceImpl@create"/utf8>>;
                <<"/api/product/save.abs"/utf8>>-><<"MProductResource.ProductResourceImpl@save"/utf8>>;
                <<"/api/product/edit.abs"/utf8>>-><<"MProductResource.ProductResourceImpl@edit"/utf8>>;
                <<"/api/product/update.abs"/utf8>>-><<"MProductResource.ProductResourceImpl@update"/utf8>>;
                <<"/api/product/delete.abs"/utf8>>-><<"MProductResource.ProductResourceImpl@delete"/utf8>>;
                <<"/api/chart-of-account/list.abs"/utf8>>-><<"MChartOfAccountResource.ChartOfAccountResourceImpl@list"/utf8>>;
                <<"/api/balance-sheet/list.abs"/utf8>>-><<"MBalanceSheetResource.BalanceSheetResourceImpl@list"/utf8>>;
                _-><<""/utf8>>;
                _ -> io:format("No match for url at framework/Route.abs:14~n"), 
                exit(dataPatternMatchFailException)
            end.
