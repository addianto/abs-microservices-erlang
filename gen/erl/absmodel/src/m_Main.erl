-module(m_Main).
-behaviour(application).
-include_lib("../include/abs_types.hrl").
-export([main/1]).
%% Application callbacks
-export([start/2, stop/1]).

'main'(Cog=#cog{ref=CogRef,dcobj=DC})->
    put(vars, #{}),
    C = none,
    Oid = null, % avoid self-call branch in synccall code
    O = #object{oid=Oid,cog=Cog},
    put(this, {state, none}),
    Stack = [DC],
     %% main.abs:40--40
    T_1 = object:new(cog:start(Cog,DC),class_MIncomeResource_IncomeResourceImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_1,<<"income"/utf8>>),
    T_1,
     %% main.abs:41--41
    T_2 = object:new(cog:start(Cog,DC),class_MIncomeModel_IncomeImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_2,<<"income-model"/utf8>>),
    T_2,
     %% main.abs:42--42
    T_3 = object:new(cog:start(Cog,DC),class_MExpenseResource_ExpenseResourceImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_3,<<"expense"/utf8>>),
    T_3,
     %% main.abs:43--43
    T_4 = object:new(cog:start(Cog,DC),class_MExpenseModel_ExpenseImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_4,<<"expense-model"/utf8>>),
    T_4,
     %% main.abs:44--44
    T_5 = object:new(cog:start(Cog,DC),class_MSummaryResource_SummaryResourceImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_5,<<"summary"/utf8>>),
    T_5,
     %% main.abs:45--45
    T_6 = object:new(cog:start(Cog,DC),class_MSummaryModel_SummaryImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_6,<<"summary-model"/utf8>>),
    T_6,
     %% main.abs:46--46
    T_7 = object:new(cog:start(Cog,DC),class_MDonorResource_DonorResourceImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_7,<<"donor"/utf8>>),
    T_7,
     %% main.abs:47--47
    T_8 = object:new(cog:start(Cog,DC),class_MDonorModel_DonorImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_8,<<"donor-model"/utf8>>),
    T_8,
     %% main.abs:48--48
    T_9 = object:new(cog:start(Cog,DC),class_MStoryBoardResource_StoryBoardResourceImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_9,<<"story-board"/utf8>>),
    T_9,
     %% main.abs:49--49
    T_10 = object:new(cog:start(Cog,DC),class_MStoryBoardModel_StoryBoardImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_10,<<"story-board-model"/utf8>>),
    T_10,
     %% main.abs:50--50
    T_11 = object:new(cog:start(Cog,DC),class_MMemberNotificationResource_MemberNotificationResourceImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_11,<<"member-notification"/utf8>>),
    T_11,
     %% main.abs:51--51
    T_12 = object:new(cog:start(Cog,DC),class_MMemberNotificationModel_MemberNotificationImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_12,<<"member-notification-model"/utf8>>),
    T_12,
     %% main.abs:52--52
    T_13 = object:new(cog:start(Cog,DC),class_MItemDonationResource_ItemDonationResourceImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_13,<<"item-donation"/utf8>>),
    T_13,
     %% main.abs:53--53
    T_14 = object:new(cog:start(Cog,DC),class_MItemDonationModel_ItemDonationImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_14,<<"item-donation-model"/utf8>>),
    T_14,
     %% main.abs:54--54
    T_15 = object:new(cog:start(Cog,DC),class_MMoneyDonationResource_MoneyDonationResourceImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_15,<<"money-donation"/utf8>>),
    T_15,
     %% main.abs:55--55
    T_16 = object:new(cog:start(Cog,DC),class_MMoneyDonationModel_MoneyDonationImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_16,<<"money-donation-model"/utf8>>),
    T_16,
     %% main.abs:56--56
    T_17 = object:new(cog:start(Cog,DC),class_MConfirmationResource_ConfirmationResourceImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_17,<<"confirmation"/utf8>>),
    T_17,
     %% main.abs:57--57
    T_18 = object:new(cog:start(Cog,DC),class_MConfirmationModel_ConfirmationImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_18,<<"confirmation-model"/utf8>>),
    T_18,
     %% main.abs:58--58
    T_19 = object:new(cog:start(Cog,DC),class_MProgramResource_ProgramResourceImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_19,<<"program"/utf8>>),
    T_19,
     %% main.abs:59--59
    T_20 = object:new(cog:start(Cog,DC),class_MProgramModel_ProgramImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_20,<<"program-model"/utf8>>),
    T_20,
     %% main.abs:60--60
    T_21 = object:new(cog:start(Cog,DC),class_MContinuousProgramResource_ContinuousProgramResourceImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_21,<<"continuous-program"/utf8>>),
    T_21,
     %% main.abs:61--61
    T_22 = object:new(cog:start(Cog,DC),class_MContinuousProgramModel_ContinuousProgramImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_22,<<"continuous-program-model"/utf8>>),
    T_22,
     %% main.abs:62--62
    T_23 = object:new(cog:start(Cog,DC),class_MEventResource_EventResourceImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_23,<<"event"/utf8>>),
    T_23,
     %% main.abs:63--63
    T_24 = object:new(cog:start(Cog,DC),class_MEventModel_EventImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_24,<<"event-model"/utf8>>),
    T_24,
     %% main.abs:64--64
    T_25 = object:new(cog:start(Cog,DC),class_MInstitutionalBeneficiaryResource_InstitutionalBeneficiaryResourceImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_25,<<"institutional-beneficiary"/utf8>>),
    T_25,
     %% main.abs:65--65
    T_26 = object:new(cog:start(Cog,DC),class_MInstitutionalBeneficiaryModel_InstitutionalBeneficiaryImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_26,<<"institutional-beneficiary-model"/utf8>>),
    T_26,
     %% main.abs:66--66
    T_27 = object:new(cog:start(Cog,DC),class_MIndividualBeneficiaryResource_IndividualBeneficiaryResourceImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_27,<<"individual-beneficiary"/utf8>>),
    T_27,
     %% main.abs:67--67
    T_28 = object:new(cog:start(Cog,DC),class_MIndividualBeneficiaryModel_IndividualBeneficiaryImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_28,<<"individual-beneficiary-model"/utf8>>),
    T_28,
     %% main.abs:68--68
    T_29 = object:new(cog:start(Cog,DC),class_MProductResource_ProductResourceImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_29,<<"product"/utf8>>),
    T_29,
     %% main.abs:69--69
    T_30 = object:new(cog:start(Cog,DC),class_MProductModel_ProductImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_30,<<"product-model"/utf8>>),
    T_30,
     %% main.abs:70--70
    T_31 = object:new(cog:start(Cog,DC),class_MChartOfAccountResource_ChartOfAccountResourceImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_31,<<"chart-of-account"/utf8>>),
    T_31,
     %% main.abs:71--71
    T_32 = object:new(cog:start(Cog,DC),class_MChartOfAccountModel_ChartOfAccountImpl,[[]],Cog,[O,DC| Stack]),
    cog_monitor:register_object_with_http_name(T_32,<<"chart-of-account-model"/utf8>>),
    T_32,
     %% main.abs:72--72
    T_33 = builtin:println(Cog,<<"Server is now running."/utf8>>),
    T_33.

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    runtime:start_link([m_Main]).

stop(_State) ->
    ok.
