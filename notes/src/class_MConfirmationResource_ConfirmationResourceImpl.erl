-module(class_MConfirmationResource_ConfirmationResourceImpl).
-include_lib("../include/abs_types.hrl").
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"ConfirmationResource">>, <<"Object">> ].

exported() -> #{ <<"detail">> => { 'm_detail', <<"Confirmation">>, [ { <<"id">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }] }, <<"list">> => { 'm_list', <<"ABS.StdLib.List<Confirmation>">>, [ ] }, <<"save">> => { 'm_save', <<"ABS.StdLib.List<Confirmation>">>, [ { <<"accountNumber">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"amount">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"programName">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }] }, <<"update">> => { 'm_update', <<"Confirmation">>, [ { <<"accountNumber">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"amount">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"programName">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }, { <<"id">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }] }, <<"delete">> => { 'm_delete', <<"ABS.StdLib.List<Confirmation>">>, [ { <<"id">>, <<"ABS.StdLib.String">>, <<"ABS.StdLib.String">>, { } }] } }.

%% --- Internal state and low-level accessors

-record(state,{'class'=class_MConfirmationResource_ConfirmationResourceImpl}).
'init_internal'()->
    #state{}.

'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

'set_val_internal'(S,S,S)->
    throw(badarg).
'get_state_for_modelapi'(S)->
    [
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% resource/ConfirmationResource.abs:22
 %% resource/ConfirmationResource.abs:22
'm_list'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% resource/ConfirmationResource.abs:23--23
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MConfirmationDbImpl_ConfirmationDbImpl,[[O,DC| Stack]])}),
         %% resource/ConfirmationResource.abs:24--24
        (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_findAll'(Callee,<<"MConfirmationModel.ConfirmationImpl_c"/utf8>>,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_className = <<"MConfirmationModel.ConfirmationImpl_c"/utf8>>,
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_findAll'(Callee, V_className,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_findAll',[<<"MConfirmationModel.ConfirmationImpl_c"/utf8>>,[]],#task_info{method= <<"findAll"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method list and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/ConfirmationResource.abs:27
 %% resource/ConfirmationResource.abs:27
'm_detail'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'id' => V_id_0 }),
    try
         %% resource/ConfirmationResource.abs:29--29
        put(vars, (get(vars))#{'condition' => iolist_to_binary([<<"id="/utf8>>, maps:get('id', get(vars))])}),
         %% resource/ConfirmationResource.abs:30--30
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MConfirmationDbImpl_ConfirmationDbImpl,[[O,DC| Stack]])}),
         %% resource/ConfirmationResource.abs:31--31
        (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_findByAttributes'(Callee,<<"MConfirmationModel.ConfirmationImpl_c"/utf8>>,maps:get('condition', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_className = <<"MConfirmationModel.ConfirmationImpl_c"/utf8>>,
                V_query = maps:get('condition', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_findByAttributes'(Callee, V_className, V_query,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_findByAttributes',[<<"MConfirmationModel.ConfirmationImpl_c"/utf8>>,maps:get('condition', get(vars)),[]],#task_info{method= <<"findByAttributes"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method detail and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/ConfirmationResource.abs:34
 %% resource/ConfirmationResource.abs:34
'm_create'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_request_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'request' => V_request_0 }),
    try
         %% resource/ConfirmationResource.abs:35--35
        []
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method create and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/ConfirmationResource.abs:38
 %% resource/ConfirmationResource.abs:38
'm_save'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_accountNumber_0,V_amount_0,V_programName_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'accountNumber' => V_accountNumber_0,
 'amount' => V_amount_0,
 'programName' => V_programName_0 }),
    try
         %% resource/ConfirmationResource.abs:39--39
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MConfirmationDbImpl_ConfirmationDbImpl,[[O,DC| Stack]])}),
         %% resource/ConfirmationResource.abs:40--40
        T_1 = (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_save'(Callee,maps:get('accountNumber', get(vars)),maps:get('amount', get(vars)),maps:get('programName', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_accountNumber = maps:get('accountNumber', get(vars)),
                V_amount = maps:get('amount', get(vars)),
                V_programName = maps:get('programName', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_save'(Callee, V_accountNumber, V_amount, V_programName,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_save',[maps:get('accountNumber', get(vars)),maps:get('amount', get(vars)),maps:get('programName', get(vars)),[]],#task_info{method= <<"save"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)(),
        T_1,
         %% resource/ConfirmationResource.abs:41--41
        (fun() -> case O of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_list'(Callee,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_list'(Callee,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_list',[[]],#task_info{method= <<"list"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method save and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/ConfirmationResource.abs:44
 %% resource/ConfirmationResource.abs:44
'm_edit'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_request_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'request' => V_request_0 }),
    try
         %% resource/ConfirmationResource.abs:45--45
        put(vars, (get(vars))#{'id' => (fun() -> case maps:get('request', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_getInput'(Callee,<<"id"/utf8>>,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_key = <<"id"/utf8>>,
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_getInput'(Callee, V_key,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_getInput',[<<"id"/utf8>>,[]],#task_info{method= <<"getInput"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% resource/ConfirmationResource.abs:46--46
        put(vars, (get(vars))#{'condition' => iolist_to_binary([<<"id="/utf8>>, maps:get('id', get(vars))])}),
         %% resource/ConfirmationResource.abs:47--47
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MConfirmationDbImpl_ConfirmationDbImpl,[[O,DC| Stack]])}),
         %% resource/ConfirmationResource.abs:48--48
        put(vars, (get(vars))#{'confirmation' => (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_findByAttributes'(Callee,<<"MConfirmationModel.ConfirmationImpl_c"/utf8>>,maps:get('condition', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_className = <<"MConfirmationModel.ConfirmationImpl_c"/utf8>>,
                V_query = maps:get('condition', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_findByAttributes'(Callee, V_className, V_query,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_findByAttributes',[<<"MConfirmationModel.ConfirmationImpl_c"/utf8>>,maps:get('condition', get(vars)),[]],#task_info{method= <<"findByAttributes"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% resource/ConfirmationResource.abs:50--50
        put(vars, (get(vars))#{'dataModel' => []}),
         %% resource/ConfirmationResource.abs:51--51
        m_ABS_StdLib_funs:f_appendright(Cog,maps:get('dataModel', get(vars)),maps:get('confirmation', get(vars)),[O,DC| Stack])
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method edit and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/ConfirmationResource.abs:54
 %% resource/ConfirmationResource.abs:54
'm_update'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_accountNumber_0,V_amount_0,V_programName_0,V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'accountNumber' => V_accountNumber_0,
 'amount' => V_amount_0,
 'programName' => V_programName_0,
 'id' => V_id_0 }),
    try
         %% resource/ConfirmationResource.abs:55--55
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MConfirmationDbImpl_ConfirmationDbImpl,[[O,DC| Stack]])}),
         %% resource/ConfirmationResource.abs:56--56
        T_1 = (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_update'(Callee,maps:get('accountNumber', get(vars)),maps:get('amount', get(vars)),maps:get('programName', get(vars)),maps:get('id', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_accountNumber = maps:get('accountNumber', get(vars)),
                V_amount = maps:get('amount', get(vars)),
                V_programName = maps:get('programName', get(vars)),
                V_id = maps:get('id', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_update'(Callee, V_accountNumber, V_amount, V_programName, V_id,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_update',[maps:get('accountNumber', get(vars)),maps:get('amount', get(vars)),maps:get('programName', get(vars)),maps:get('id', get(vars)),[]],#task_info{method= <<"update"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)(),
        T_1,
         %% resource/ConfirmationResource.abs:57--57
        (fun() -> case O of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_detail'(Callee,maps:get('id', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_id = maps:get('id', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_detail'(Callee, V_id,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_detail',[maps:get('id', get(vars)),[]],#task_info{method= <<"detail"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method update and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% resource/ConfirmationResource.abs:60
 %% resource/ConfirmationResource.abs:60
'm_delete'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'id' => V_id_0 }),
    try
         %% resource/ConfirmationResource.abs:62--62
        put(vars, (get(vars))#{'condition' => iolist_to_binary([<<"id="/utf8>>, maps:get('id', get(vars))])}),
         %% resource/ConfirmationResource.abs:63--63
        put(vars, (get(vars))#{'orm' => object:new_local(O, Cog,class_MConfirmationDbImpl_ConfirmationDbImpl,[[O,DC| Stack]])}),
         %% resource/ConfirmationResource.abs:64--64
        put(vars, (get(vars))#{'confirmation' => (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_findByAttributes'(Callee,<<"MConfirmationModel.ConfirmationImpl_c"/utf8>>,maps:get('condition', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_className = <<"MConfirmationModel.ConfirmationImpl_c"/utf8>>,
                V_query = maps:get('condition', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_findByAttributes'(Callee, V_className, V_query,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_findByAttributes',[<<"MConfirmationModel.ConfirmationImpl_c"/utf8>>,maps:get('condition', get(vars)),[]],#task_info{method= <<"findByAttributes"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% resource/ConfirmationResource.abs:66--66
        T_1 = (fun() -> case maps:get('orm', get(vars)) of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_delete'(Callee,maps:get('condition', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_condition = maps:get('condition', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_delete'(Callee, V_condition,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_delete',[maps:get('condition', get(vars)),[]],#task_info{method= <<"delete"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)(),
        T_1,
         %% resource/ConfirmationResource.abs:67--67
        (fun() -> case O of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_list'(Callee,[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_list'(Callee,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_list',[[]],#task_info{method= <<"list"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method delete and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
