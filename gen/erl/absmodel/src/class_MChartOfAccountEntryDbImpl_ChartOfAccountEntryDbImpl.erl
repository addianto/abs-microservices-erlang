-module(class_MChartOfAccountEntryDbImpl_ChartOfAccountEntryDbImpl).
-include_lib("../include/abs_types.hrl").
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"ChartOfAccountEntryDb">>, <<"Object">> ].

exported() -> #{  }.

%% --- Internal state and low-level accessors

-record(state,{'class'=class_MChartOfAccountEntryDbImpl_ChartOfAccountEntryDbImpl}).
'init_internal'()->
    #state{}.

'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

'set_val_internal'(S,S,S)->
    throw(badarg).
'get_state_for_modelapi'(S)->
    [
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% repository/ChartOfAccountEntryDb.abs:22
 %% repository/ChartOfAccountEntryDb.abs:22
'm_findAllByAttributes'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_className_0,V_query_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'className' => V_className_0,
 'query' => V_query_0 }),
    try
         %% repository/ChartOfAccountEntryDb.abs:23--23
        []
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method findAllByAttributes and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/ChartOfAccountEntryDb.abs:25
 %% repository/ChartOfAccountEntryDb.abs:25
'm_findAll'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_className_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'className' => V_className_0 }),
    try
         %% repository/ChartOfAccountEntryDb.abs:26--26
        []
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method findAll and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/ChartOfAccountEntryDb.abs:28
 %% repository/ChartOfAccountEntryDb.abs:28
'm_findByAttributes'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_className_0,V_query_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'className' => V_className_0,
 'query' => V_query_0 }),
    try
         %% repository/ChartOfAccountEntryDb.abs:29--29
        object:new_local(O, Cog,class_MChartOfAccountEntryModel_ChartOfAccountEntryImpl,[[O,DC| Stack]])
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method findByAttributes and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/ChartOfAccountEntryDb.abs:31
 %% repository/ChartOfAccountEntryDb.abs:31
'm_find'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_className_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'className' => V_className_0 }),
    try
         %% repository/ChartOfAccountEntryDb.abs:32--32
        object:new_local(O, Cog,class_MChartOfAccountEntryModel_ChartOfAccountEntryImpl,[[O,DC| Stack]])
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method find and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/ChartOfAccountEntryDb.abs:34
 %% repository/ChartOfAccountEntryDb.abs:34
'm_save'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_object_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'object' => V_object_0 }),
    try
        ok,
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method save and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/ChartOfAccountEntryDb.abs:36
 %% repository/ChartOfAccountEntryDb.abs:36
'm_delete'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_object_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'object' => V_object_0 }),
    try
        ok,
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method delete and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/ChartOfAccountEntryDb.abs:38
 %% repository/ChartOfAccountEntryDb.abs:38
'm_update'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_object_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'object' => V_object_0 }),
    try
         %% repository/ChartOfAccountEntryDb.abs:39--39
        object:new_local(O, Cog,class_MChartOfAccountEntryModel_ChartOfAccountEntryImpl,[[O,DC| Stack]])
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method update and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% repository/ChartOfAccountEntryDb.abs:41
 %% repository/ChartOfAccountEntryDb.abs:41
'm_log'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_log_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'log' => V_log_0 }),
    try
         %% repository/ChartOfAccountEntryDb.abs:42--42
        <<"return log"/utf8>>
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method log and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
