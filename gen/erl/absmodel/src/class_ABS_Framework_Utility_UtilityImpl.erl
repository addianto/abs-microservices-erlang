-module(class_ABS_Framework_Utility_UtilityImpl).
-include_lib("../include/abs_types.hrl").
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"Utility">>, <<"Object">> ].

exported() -> #{  }.

%% --- Internal state and low-level accessors

-record(state,{'class'=class_ABS_Framework_Utility_UtilityImpl}).
'init_internal'()->
    #state{}.

'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

'set_val_internal'(S,S,S)->
    throw(badarg).
'get_state_for_modelapi'(S)->
    [
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% framework/Utility.abs:12
 %% framework/Utility.abs:12
'm_stringToInteger'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_s_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 's' => V_s_0 }),
    try
         %% framework/Utility.abs:13--13
        put(vars, (get(vars))#{'inputString' => maps:get('s', get(vars))}),
         %% framework/Utility.abs:14--14
        put(vars, (get(vars))#{'length' => builtin:strlen(Cog,maps:get('inputString', get(vars)))}),
         %% framework/Utility.abs:15--15
        put(vars, (get(vars))#{'output' => 0}),
         %% framework/Utility.abs:17--17
        case cmp:lt(maps:get('length', get(vars)),0) of
            true ->  %% framework/Utility.abs:18--18
            throw(dataPatternMatchFailException);
            false ->          %% framework/Utility.abs:21--21
        put(vars, (get(vars))#{'negative' => (fun() -> case O of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_isNegativeNumber'(Callee,maps:get('inputString', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_s = maps:get('inputString', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_isNegativeNumber'(Callee, V_s,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_isNegativeNumber',[maps:get('inputString', get(vars)),[]],#task_info{method= <<"isNegativeNumber"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% framework/Utility.abs:23--23
        case maps:get('negative', get(vars)) of
            true ->  %% framework/Utility.abs:24--24
            put(vars, (get(vars))#{'inputString' := builtin:substr(Cog,maps:get('inputString', get(vars)),1,(maps:get('length', get(vars)) - 1) )});
            false ->         ok
        end,
         %% framework/Utility.abs:27--27
        put(vars, (get(vars))#{'length' := builtin:strlen(Cog,maps:get('inputString', get(vars)))}),
         %% framework/Utility.abs:28--28
        put(vars, (get(vars))#{'idx' => 0}),
         %% framework/Utility.abs:29--29
        put(vars, (get(vars))#{'tens' => (fun() -> case O of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_power'(Callee,1,maps:get('length', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_a = 1,
                V_b = maps:get('length', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_power'(Callee, V_a, V_b,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_power',[1,maps:get('length', get(vars)),[]],#task_info{method= <<"power"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% framework/Utility.abs:31--31
        []=(fun Loop ([])->
            case cmp:lt(maps:get('idx', get(vars)),maps:get('length', get(vars))) of
            false -> [];
            true -> receive
                    {stop_world, CogRef} ->
                        cog:task_is_blocked_for_gc(Cog, self(), get(task_info), get(this)),
                        cog:task_is_runnable(Cog,self()),
                        task:wait_for_token(Cog,[O,DC| Stack])
                    after 0 -> ok
                end,
                 %% framework/Utility.abs:33--33
                put(vars, (get(vars))#{'ch' => builtin:substr(Cog,maps:get('inputString', get(vars)),0,1)}),
                 %% framework/Utility.abs:34--34
                put(vars, (get(vars))#{'digit' => (fun() -> case O of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_parseDigit'(Callee,maps:get('ch', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_ch = maps:get('ch', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_parseDigit'(Callee, V_ch,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_parseDigit',[maps:get('ch', get(vars)),[]],#task_info{method= <<"parseDigit"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)()}),
                 %% framework/Utility.abs:37--37
                put(vars, (get(vars))#{'temp' => (maps:get('digit', get(vars)) * maps:get('tens', get(vars))) }),
                 %% framework/Utility.abs:40--40
                put(vars, (get(vars))#{'output' := (maps:get('output', get(vars)) + maps:get('temp', get(vars))) }),
                 %% framework/Utility.abs:43--43
                put(vars, (get(vars))#{'idx' := (maps:get('idx', get(vars)) + 1) }),
                 %% framework/Utility.abs:47--47
                put(vars, (get(vars))#{'remainingLength' => builtin:strlen(Cog,maps:get('inputString', get(vars)))}),
                 %% framework/Utility.abs:48--48
                put(vars, (get(vars))#{'inputString' := builtin:substr(Cog,maps:get('inputString', get(vars)),1,(maps:get('remainingLength', get(vars)) - 1) )}),
            Loop([])  end end)
        ([]),
         %% framework/Utility.abs:51--51
        case maps:get('negative', get(vars)) of
            true ->  %% framework/Utility.abs:52--52
            put(vars, (get(vars))#{'output' := (maps:get('output', get(vars)) * -1) });
            false ->         ok
        end
        end,
         %% framework/Utility.abs:56--56
        maps:get('output', get(vars))
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method stringToInteger and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% framework/Utility.abs:59
 %% framework/Utility.abs:59
'm_stringToRational'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_s_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 's' => V_s_0 }),
    try
         %% framework/Utility.abs:60--60
        put(vars, (get(vars))#{'inputString' => maps:get('s', get(vars))}),
         %% framework/Utility.abs:61--61
        put(vars, (get(vars))#{'length' => builtin:strlen(Cog,maps:get('inputString', get(vars)))}),
         %% framework/Utility.abs:62--62
        put(vars, (get(vars))#{'output' => 0}),
         %% framework/Utility.abs:64--64
        case cmp:lt(maps:get('length', get(vars)),1) of
            true ->  %% framework/Utility.abs:65--65
            put(vars, (get(vars))#{'output' := -1});
            false ->          %% framework/Utility.abs:68--68
        put(vars, (get(vars))#{'negative' => (fun() -> case O of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_isNegativeNumber'(Callee,maps:get('inputString', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_s = maps:get('inputString', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_isNegativeNumber'(Callee, V_s,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_isNegativeNumber',[maps:get('inputString', get(vars)),[]],#task_info{method= <<"isNegativeNumber"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% framework/Utility.abs:70--70
        case maps:get('negative', get(vars)) of
            true ->  %% framework/Utility.abs:71--71
            put(vars, (get(vars))#{'inputString' := builtin:substr(Cog,maps:get('inputString', get(vars)),1,(maps:get('length', get(vars)) - 1) )});
            false ->         ok
        end,
         %% framework/Utility.abs:74--74
        put(vars, (get(vars))#{'length' := builtin:strlen(Cog,maps:get('inputString', get(vars)))}),
         %% framework/Utility.abs:75--75
        put(vars, (get(vars))#{'idx' => 0}),
         %% framework/Utility.abs:76--76
        put(vars, (get(vars))#{'tens' => (fun() -> case O of
            null -> throw(dataNullPointerException);
            Callee=#object{oid=Oid,cog=Cog} ->
                %% self-call
                Vars=get(vars),
                Result=C:'m_power'(Callee,1,maps:get('length', get(vars)),[O,DC,Vars| Stack]),
                put(vars, Vars),
                Result;
            Callee=#object{oid=ObjRef,cog=Cog} ->
                %% cog-local call
                V_a = 1,
                V_b = maps:get('length', get(vars)),
                State=get(this),
                Vars=get(vars),
                cog:object_state_changed(Cog, O, State),
                put(this,cog:get_object_state(Callee#object.cog, Callee)),
                put(task_info,(get(task_info))#task_info{this=Callee}),
                T=object:get_class_from_state(get(this)), % it's the callee state already
                Result=T:'m_power'(Callee, V_a, V_b,[O,DC,Vars,State| Stack]),
                cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                put(task_info,(get(task_info))#task_info{this=O}),
                put(this, cog:get_object_state(Cog, O)),
                put(vars, Vars),
                Result;
            Callee ->
                %% remote call
                TempFuture = future:start(Callee,'m_power',[1,maps:get('length', get(vars)),[]],#task_info{method= <<"power"/utf8>>},Cog,[O,DC| Stack]),
                future:get_blocking(TempFuture, Cog, [O,DC| Stack])
        end end)()}),
         %% framework/Utility.abs:78--78
        []=(fun Loop ([])->
            case cmp:lt(maps:get('idx', get(vars)),maps:get('length', get(vars))) of
            false -> [];
            true -> receive
                    {stop_world, CogRef} ->
                        cog:task_is_blocked_for_gc(Cog, self(), get(task_info), get(this)),
                        cog:task_is_runnable(Cog,self()),
                        task:wait_for_token(Cog,[O,DC| Stack])
                    after 0 -> ok
                end,
                 %% framework/Utility.abs:80--80
                put(vars, (get(vars))#{'ch' => builtin:substr(Cog,maps:get('inputString', get(vars)),0,1)}),
                 %% framework/Utility.abs:81--81
                put(vars, (get(vars))#{'digit' => (fun() -> case O of
                    null -> throw(dataNullPointerException);
                    Callee=#object{oid=Oid,cog=Cog} ->
                        %% self-call
                        Vars=get(vars),
                        Result=C:'m_parseDigit'(Callee,maps:get('ch', get(vars)),[O,DC,Vars| Stack]),
                        put(vars, Vars),
                        Result;
                    Callee=#object{oid=ObjRef,cog=Cog} ->
                        %% cog-local call
                        V_ch = maps:get('ch', get(vars)),
                        State=get(this),
                        Vars=get(vars),
                        cog:object_state_changed(Cog, O, State),
                        put(this,cog:get_object_state(Callee#object.cog, Callee)),
                        put(task_info,(get(task_info))#task_info{this=Callee}),
                        T=object:get_class_from_state(get(this)), % it's the callee state already
                        Result=T:'m_parseDigit'(Callee, V_ch,[O,DC,Vars,State| Stack]),
                        cog:object_state_changed(Callee#object.cog, Callee, get(this)),
                        put(task_info,(get(task_info))#task_info{this=O}),
                        put(this, cog:get_object_state(Cog, O)),
                        put(vars, Vars),
                        Result;
                    Callee ->
                        %% remote call
                        TempFuture = future:start(Callee,'m_parseDigit',[maps:get('ch', get(vars)),[]],#task_info{method= <<"parseDigit"/utf8>>},Cog,[O,DC| Stack]),
                        future:get_blocking(TempFuture, Cog, [O,DC| Stack])
                end end)()}),
                 %% framework/Utility.abs:84--84
                put(vars, (get(vars))#{'temp' => (maps:get('digit', get(vars)) * maps:get('tens', get(vars))) }),
                 %% framework/Utility.abs:87--87
                put(vars, (get(vars))#{'output' := ( rationals:add(maps:get('output', get(vars)),maps:get('temp', get(vars)))) }),
                 %% framework/Utility.abs:90--90
                put(vars, (get(vars))#{'idx' := (maps:get('idx', get(vars)) + 1) }),
                 %% framework/Utility.abs:94--94
                put(vars, (get(vars))#{'remainingLength' => builtin:strlen(Cog,maps:get('inputString', get(vars)))}),
                 %% framework/Utility.abs:95--95
                put(vars, (get(vars))#{'inputString' := builtin:substr(Cog,maps:get('inputString', get(vars)),1,(maps:get('remainingLength', get(vars)) - 1) )}),
            Loop([])  end end)
        ([]),
         %% framework/Utility.abs:98--98
        case maps:get('negative', get(vars)) of
            true ->  %% framework/Utility.abs:99--99
            put(vars, (get(vars))#{'output' := ( rationals:mul(maps:get('output', get(vars)),-1)) });
            false ->         ok
        end
        end,
         %% framework/Utility.abs:103--103
        maps:get('output', get(vars))
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method stringToRational and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% framework/Utility.abs:106
 %% framework/Utility.abs:106
'm_stringToBoolean'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_s_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 's' => V_s_0 }),
    try
         %% framework/Utility.abs:107--107
        (cmp:eq(maps:get('s', get(vars)),<<"True"/utf8>>)) or (cmp:eq(maps:get('s', get(vars)),<<"true"/utf8>>))
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method stringToBoolean and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% framework/Utility.abs:110
 %% framework/Utility.abs:110
'm_power'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_a_0,V_b_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'a' => V_a_0,
 'b' => V_b_0 }),
    try
         %% framework/Utility.abs:111--111
        put(vars, (get(vars))#{'result' => maps:get('a', get(vars))}),
         %% framework/Utility.abs:112--112
        put(vars, (get(vars))#{'n' => maps:get('b', get(vars))}),
         %% framework/Utility.abs:114--114
        []=(fun Loop ([])->
            case cmp:gt(maps:get('n', get(vars)),1) of
            false -> [];
            true -> receive
                    {stop_world, CogRef} ->
                        cog:task_is_blocked_for_gc(Cog, self(), get(task_info), get(this)),
                        cog:task_is_runnable(Cog,self()),
                        task:wait_for_token(Cog,[O,DC| Stack])
                    after 0 -> ok
                end,
                 %% framework/Utility.abs:115--115
                put(vars, (get(vars))#{'result' := (maps:get('result', get(vars)) * 10) }),
                 %% framework/Utility.abs:116--116
                put(vars, (get(vars))#{'n' := (maps:get('n', get(vars)) - 1) }),
            Loop([])  end end)
        ([]),
         %% framework/Utility.abs:119--119
        maps:get('result', get(vars))
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method power and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% framework/Utility.abs:122
 %% framework/Utility.abs:122
'm_parseDigit'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_ch_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'ch' => V_ch_0 }),
    try
         %% framework/Utility.abs:123--135
        put(vars, (get(vars))#{'result' => begin
            case maps:get('ch', get(vars)) of
                <<"0"/utf8>>->0;
                <<"1"/utf8>>->1;
                <<"2"/utf8>>->2;
                <<"3"/utf8>>->3;
                <<"4"/utf8>>->4;
                <<"5"/utf8>>->5;
                <<"6"/utf8>>->6;
                <<"7"/utf8>>->7;
                <<"8"/utf8>>->8;
                <<"9"/utf8>>->9;
                _->-1;
                _ -> io:format("No match for ch at framework/Utility.abs:123~n"), 
                exit(dataPatternMatchFailException)
            end
        end}),
         %% framework/Utility.abs:137--137
        case cmp:eq(maps:get('result', get(vars)),-1) of
            true ->  %% framework/Utility.abs:138--138
            throw(dataPatternMatchFailException);
            false ->         ok
        end,
         %% framework/Utility.abs:140--140
        maps:get('result', get(vars))
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method parseDigit and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% framework/Utility.abs:143
 %% framework/Utility.abs:143
'm_isNegativeNumber'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_s_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 's' => V_s_0 }),
    try
         %% framework/Utility.abs:144--144
        put(vars, (get(vars))#{'length' => builtin:strlen(Cog,maps:get('s', get(vars)))}),
         %% framework/Utility.abs:145--145
        put(vars, (get(vars))#{'firstDigit' => builtin:substr(Cog,maps:get('s', get(vars)),0,1)}),
         %% framework/Utility.abs:146--146
        put(vars, (get(vars))#{'result' => false}),
         %% framework/Utility.abs:148--148
        cmp:eq(maps:get('firstDigit', get(vars)),<<"-"/utf8>>)
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method isNegativeNumber and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
