# ABS-Microservices Framework berbasis Erlang OTP

## Prekondisi:
- ^[Erlang/OTP 22](https://www.erlang-solutions.com/resources/download.html)
- ^[Java 8](https://adoptopenjdk.net)
- [ABS](https://abs-models.org/getting_started/local-install/)
- Buat database (untuk mempermudah, dapat meng-clone repository https://gitlab.com/RSE-Lab-Fasilkom-UI/prices-2/abs-microservices, kemudian lakukan build.).

Jika semua prekondisi sudah ada, aplikasi langsung dapat dijalankan tanpa compile ABS dengan menjalankan `gen/erl/run -p 8080` atau `./run.sh` pada base directory.

Akan tetapi jika ingin compile ABS, bisa dilakukan dengan langkah-langkah berikut:
1. Compile ABS:
```
// on base directory
absc --erlang **/*.abs

// runserver (optional)
gen/erl/run -p 8080
```

Route URL yang dihasilkan masih berupa default dari ABS, contoh URLnya: `localhost:8080/call/chart-of-account/list`. Konfigurasi untuk URL ini terdapat pada file modelapi_v2.erl, contohnya terdapat di notes/modelapi_v2.erl. Langkah berikut untuk meng-copy file konfigurasi yang dibutuhkan ke dalam folder hasil generasi.

2. Konfigurasi route & autentikasi (modelapi_v2.erl):
```
// on base directory
./modelapi_v2.sh

// recompile erlang then runserver (optional)
./run.sh
```

Sekarang format URL sudah sesuai dengan format ABS-Microservices Framework: `localhost:8080/api/chart-of-account/list.abs`. 
Selanjutnya perlu Copy library-library dan file ORM.

3. Copy library dan kode ORM:
```
// on base directory
./put.sh
./orm.sh
```

Aplikasi Erlang yang digenerasi ABS masih belum terhubung dengan basis data, harus ditambah secara manual ke dalam file hasil generasinya (repository). 

4. Sesuaikan isi generated repository:
```
// example: Chart of Account
// open class_MChartOfAccountDbImpl_ChartOfAccountDbImpl.erl
// find 'm_findAll' function, below '%% repository/ChartOfAccountDb.abs:23'.
// comment the elbow brackets '[]'.
// add this below the commented elbow brackets.
orm:findAll('chartofaccountimpl)
```

Sekarang sudah terhubung dengan basis data, tetapi hanya pada Chart of Account dengan method list.

Semua model yang dibuat di dalam kode ABS harus ditambahkan kode orm pada file repository hasil generasi ABS (cth: class_MChartOfAccountDbImpl_ChartOfAccountDbImpl.erl).
Untuk mempercepat proses ini bisa Copy kode erlang dan kode hasil compile erlang ke dalam folder gen/erl/absmodel.

5. Copy kode erlang dengan ORM:
```
// on base directory
./precompiled.sh
```

## Menjalankan aplikasi:
```
// on base directory

// just run, no erlang compile
gen/erl/run -p 8080

// recompile erlang then run
./run.sh
```

## Menjalankan aplikasi setelah membuat perubahan kode Erlang:
1. Compile kode Erlang.
```
cd gen/erl/absmodel
erl -make
```
2. Jalankan aplikasi
```
cd ../../../
gen/erl/run -p 8080
```
atau jalankan ini pada root dir.
```
./run.sh
```

### Library Eksternal:
### 1. [epgsql](https://github.com/epgsql/epgsql) - Penghubung basis data MySQL.

### 2. [jiffy](https://github.com/davisp/jiffy) - JSON Parser untuk Erlang.

### 3. [base64url](https://github.com/potatosalad/erlang-base64url) - Decode & Encode base64.

# Kontak
- priambudibagaskara@gmail.com
