-module(orm).

-include_lib("../deps/epgsql/include/epgsql.hrl").

-compile(export_all).

saveProduct(Table, Name, Description) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Result = epgsql:squery(C,
			   "insert into " ++
			     Table ++
			       " (NAME, DESCRIPTION) values " ++
				 "('" ++
				   Name ++ "', '" ++ Description ++ "');"),
    epgsql:close(C),
    Result.

updateProduct(Table, NameValue, DescValue, Condition) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Q = "update " ++
	  Table ++
	    " SET " ++
	      "name" ++
		" = '" ++
		  NameValue ++
		    "', " ++
		      "description" ++
			"= '" ++ DescValue ++ "' WHERE " ++ Condition ++ ";",
    Result = epgsql:squery(C, Q),
    epgsql:close(C),
    Result.

saveExpense(Table, Datestamp, Description, Amount,
	    ProgramName, IdCoa) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Result = epgsql:squery(C,
			   "insert into " ++
			     Table ++
			       " (Datestamp, Description, Amount, ProgramName"
			       ", IdCoa) values "
				 ++
				 "('" ++
				   Datestamp ++
				     "', '" ++
				       Description ++
					 "', '" ++
					   Amount ++
					     "', '" ++
					       ProgramName ++
						 "', '" ++ IdCoa ++ "');"),
    epgsql:close(C),
    Result.

updateExpense(Table, Datestamp, Description, Amount,
	      ProgramName, IdCoa, Condition) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Q = "update " ++
	  Table ++
	    " SET " ++
	      "datestamp" ++
		" = '" ++
		  Datestamp ++
		    "', " ++
		      "amount" ++
			" = '" ++
			  Amount ++
			    "', " ++
			      "programname" ++
				" = '" ++
				  ProgramName ++
				    "', " ++
				      "idcoa" ++
					" = '" ++
					  IdCoa ++
					    "', " ++
					      "description" ++
						"= '" ++
						  Description ++
						    "' WHERE " ++
						      Condition ++ ";",
    Result = epgsql:squery(C, Q),
    epgsql:close(C),
    Result.

saveDonor(Table, Name, Email, Phone, Address) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Result = epgsql:squery(C,
			   "insert into " ++
			     Table ++
			       " (Name, Email, Phone, Address) values " ++
				 "('" ++
				   Name ++
				     "', '" ++
				       Email ++
					 "', '" ++
					   Phone ++ "', '" ++ Address ++ "');"),
    epgsql:close(C),
    Result.

updateDonor(Table, Name, Email, Phone, Address,
	    Condition) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Q = "update " ++
	  Table ++
	    " SET " ++
	      "name" ++
		" = '" ++
		  Name ++
		    "', " ++
		      "email" ++
			" = '" ++
			  Email ++
			    "', " ++
			      "phone" ++
				" = '" ++
				  Phone ++
				    "', " ++
				      "address" ++
					" = '" ++
					  Address ++
					    "' WHERE " ++ Condition ++ ";",
    Result = epgsql:squery(C, Q),
    epgsql:close(C),
    Result.

saveStoryBoard(Table, Title, Content, PublishDate,
	       ThumbnailUrl, ProgramName) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Result = epgsql:squery(C,
			   "insert into " ++
			     Table ++
			       " (Title, Content, PublishDate, ThumbnailUrl, "
			       "ProgramName) values "
				 ++
				 "('" ++
				   Title ++
				     "', '" ++
				       Content ++
					 "', '" ++
					   PublishDate ++
					     "', '" ++
					       ThumbnailUrl ++
						 "', '" ++
						   ProgramName ++ "');"),
    epgsql:close(C),
    Result.

updateStoryBoard(Table, Title, Content, PublishDate,
		 ThumbnailUrl, ProgramName, Condition) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Q = "update " ++
	  Table ++
	    " SET " ++
	      "title" ++
		" = '" ++
		  Title ++
		    "', " ++
		      "content" ++
			" = '" ++
			  Content ++
			    "', " ++
			      "publishdate" ++
				" = '" ++
				  PublishDate ++
				    "', " ++
				      "thumbnailurl" ++
					" = '" ++
					  ThumbnailUrl ++
					    "', " ++
					      "programname" ++
						" = '" ++
						  ProgramName ++
						    "' WHERE " ++
						      Condition ++ ";",
    Result = epgsql:squery(C, Q),
    epgsql:close(C),
    Result.

saveNotification(Table, Title, Content, PublishDate,
		 ThumbnailUrl, IdDonor) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Result = epgsql:squery(C,
			   "insert into " ++
			     Table ++
			       " (Title, Content, PublishDate, ThumbnailUrl, "
			       "iddonor) values "
				 ++
				 "('" ++
				   Title ++
				     "', '" ++
				       Content ++
					 "', '" ++
					   PublishDate ++
					     "', '" ++
					       ThumbnailUrl ++
						 "', '" ++ IdDonor ++ "');"),
    epgsql:close(C),
    Result.

updateNotification(Table, Title, Content, PublishDate,
		   ThumbnailUrl, IdDonor, Condition) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Q = "update " ++
	  Table ++
	    " SET " ++
	      "title" ++
		" = '" ++
		  Title ++
		    "', " ++
		      "content" ++
			" = '" ++
			  Content ++
			    "', " ++
			      "publishdate" ++
				" = '" ++
				  PublishDate ++
				    "', " ++
				      "thumbnailurl" ++
					" = '" ++
					  ThumbnailUrl ++
					    "', " ++
					      "iddonor" ++
						" = '" ++
						  IdDonor ++
						    "' WHERE " ++
						      Condition ++ ";",
    Result = epgsql:squery(C, Q),
    epgsql:close(C),
    Result.

saveItemDonation(Table, ItemName, Quantity,
		 ProgramName) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Result = epgsql:squery(C,
			   "insert into " ++
			     Table ++
			       " (ItemName, Quantity, ProgramName) values " ++
				 "('" ++
				   ItemName ++
				     "', '" ++
				       Quantity ++
					 "', '" ++ ProgramName ++ "');"),
    epgsql:close(C),
    Result.

updateItemDonation(Table, ItemName, Quantity,
		   ProgramName, Condition) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Q = "update " ++
	  Table ++
	    " SET " ++
	      "itemname" ++
		" = '" ++
		  ItemName ++
		    "', " ++
		      "quantity" ++
			" = '" ++
			  Quantity ++
			    "', " ++
			      "programname" ++
				" = '" ++
				  ProgramName ++ "' WHERE " ++ Condition ++ ";",
    Result = epgsql:squery(C, Q),
    epgsql:close(C),
    Result.

saveMoneyDonation(Table, Amount, AccountNumber,
		  ProgramName) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Result = epgsql:squery(C,
			   "insert into " ++
			     Table ++
			       " (Amount, AccountNumber, ProgramName) "
			       "values "
				 ++
				 "('" ++
				   Amount ++
				     "', '" ++
				       AccountNumber ++
					 "', '" ++ ProgramName ++ "');"),
    epgsql:close(C),
    Result.

updateMoneyDonation(Table, Amount, AccountNumber,
		    ProgramName, Condition) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Q = "update " ++
	  Table ++
	    " SET " ++
	      "amount" ++
		" = '" ++
		  Amount ++
		    "', " ++
		      "accountnumber" ++
			" = '" ++
			  AccountNumber ++
			    "', " ++
			      "programname" ++
				" = '" ++
				  ProgramName ++ "' WHERE " ++ Condition ++ ";",
    Result = epgsql:squery(C, Q),
    epgsql:close(C),
    Result.

saveConfirmation(Table, Amount, AccountNumber,
		 ProgramName) ->
    saveMoneyDonation(Table, Amount, AccountNumber,
		      ProgramName).

updateConfirmation(Table, Amount, AccountNumber,
		   ProgramName, Condition) ->
    updateMoneyDonation(Table, Amount, AccountNumber,
			ProgramName, Condition).

saveProgram(Table, Partner, Name, Description, Logourl,
	    Target, Frequency) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Result = epgsql:squery(C,
			   "insert into " ++
			     Table ++
			       " (Partner, Name, Description, Logourl, "
			       "Target, Frequency) values "
				 ++
				 "('" ++
				   Partner ++
				     "', '" ++
				       Name ++
					 "', '" ++
					   Description ++
					     "', '" ++
					       Logourl ++
						 "', '" ++
						   Target ++
						     "', '" ++
						       Frequency ++ "');"),
    epgsql:close(C),
    Result.

updateProgram(Table, Partner, Name, Description,
	      Logourl, Target, Frequency, Condition) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Q = "update " ++
	  Table ++
	    " SET " ++
	      "partner" ++
		" = '" ++
		  Partner ++
		    "', " ++
		      "name" ++
			" = '" ++
			  Name ++
			    "', " ++
			      "description" ++
				" = '" ++
				  Description ++
				    "', " ++
				      "logourl" ++
					" = '" ++
					  Logourl ++
					    "', " ++
					      "target" ++
						" = '" ++
						  Target ++
						    "', " ++
						      "frequency" ++
							" = '" ++
							  Frequency ++
							    "' WHERE " ++
							      Condition ++ ";",
    Result = epgsql:squery(C, Q),
    epgsql:close(C),
    Result.

saveContinousProgram(Table, Partner, Name, Description,
		     Logourl, Target, Frequency) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Result = epgsql:squery(C,
			   "insert into " ++
			     Table ++
			       " (Partner, Name, Description, Logourl, "
			       "Target, Frequency) values "
				 ++
				 "('" ++
				   Partner ++
				     "', '" ++
				       Name ++
					 "', '" ++
					   Description ++
					     "', '" ++
					       Logourl ++
						 "', '" ++
						   Target ++
						     "', '" ++
						       Frequency ++ "');"),
    epgsql:close(C),
    Result.

updateContinousProgram(Table, Partner, Name,
		       Description, Logourl, Target, Frequency, Condition) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Q = "update " ++
	  Table ++
	    " SET " ++
	      "partner" ++
		" = '" ++
		  Partner ++
		    "', " ++
		      "name" ++
			" = '" ++
			  Name ++
			    "', " ++
			      "description" ++
				" = '" ++
				  Description ++
				    "', " ++
				      "logourl" ++
					" = '" ++
					  Logourl ++
					    "', " ++
					      "target" ++
						" = '" ++
						  Target ++
						    "', " ++
						      "frequency" ++
							" = '" ++
							  Frequency ++
							    "' WHERE " ++
							      Condition ++ ";",
    Result = epgsql:squery(C, Q),
    epgsql:close(C),
    Result.

saveEvent(Table, Partner, Name, Description, Logourl,
	  Eventdate, Target) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Result = epgsql:squery(C,
			   "insert into " ++
			     Table ++
			       " (Partner, Name, Description, Logourl, "
			       "Eventdate, Target) values "
				 ++
				 "('" ++
				   Partner ++
				     "', '" ++
				       Name ++
					 "', '" ++
					   Description ++
					     "', '" ++
					       Logourl ++
						 "', '" ++
						   Eventdate ++
						     "', '" ++ Target ++ "');"),
    epgsql:close(C),
    Result.

updateEvent(Table, Partner, Name, Description, Logourl,
	    Eventdate, Target, Condition) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Q = "update " ++
	  Table ++
	    " SET " ++
	      "partner" ++
		" = '" ++
		  Partner ++
		    "', " ++
		      "name" ++
			" = '" ++
			  Name ++
			    "', " ++
			      "description" ++
				" = '" ++
				  Description ++
				    "', " ++
				      "logourl" ++
					" = '" ++
					  Logourl ++
					    "', " ++
					      "eventdate" ++
						" = '" ++
						  Eventdate ++
						    "', " ++
						      "target" ++
							" = '" ++
							  Target ++
							    "' WHERE " ++
							      Condition ++ ";",
    Result = epgsql:squery(C, Q),
    epgsql:close(C),
    Result.

saveInstitutionalBeneficiary(Table, Address, Phone,
			     Name, Email) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Result = epgsql:squery(C,
			   "insert into " ++
			     Table ++
			       " (Address, Phone, Name, Email) values " ++
				 "('" ++
				   Address ++
				     "', '" ++
				       Phone ++
					 "', '" ++
					   Name ++ "', '" ++ Email ++ "');"),
    epgsql:close(C),
    Result.

updateInstitutionalBeneficiary(Table, Address, Phone,
			       Name, Email, Condition) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Q = "update " ++
	  Table ++
	    " SET " ++
	      "address" ++
		" = '" ++
		  Address ++
		    "', " ++
		      "phone" ++
			" = '" ++
			  Phone ++
			    "', " ++
			      "name" ++
				" = '" ++
				  Name ++
				    "', " ++
				      "email" ++
					" = '" ++
					  Email ++
					    "' WHERE " ++ Condition ++ ";",
    Result = epgsql:squery(C, Q),
    epgsql:close(C),
    Result.

saveIndividualBeneficiary(Table, Address, Name) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Result = epgsql:squery(C,
			   "insert into " ++
			     Table ++
			       " (Address, Name) values " ++
				 "('" ++ Address ++ "', '" ++ Name ++ "');"),
    epgsql:close(C),
    Result.

updateIndividualBeneficiary(Table, Address, Name,
			    Condition) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Q = "update " ++
	  Table ++
	    " SET " ++
	      "address" ++
		" = '" ++
		  Address ++
		    "', " ++
		      "name" ++
			" = '" ++ Name ++ "' WHERE " ++ Condition ++ ";",
    Result = epgsql:squery(C, Q),
    epgsql:close(C),
    Result.

saveIncome(Table, Datestamp, Description, Amount,
	   ProgramName, IdCoa) ->
    saveExpense(Table, Datestamp, Description, Amount,
		ProgramName, IdCoa).

updateIncome(Table, Datestamp, Description, Amount,
	     ProgramName, IdCoa, Condition) ->
    updateExpense(Table, Datestamp, Description, Amount,
		  ProgramName, IdCoa, Condition).

delete(Table, Condition) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Q = "delete from " ++
	  Table ++ " where " ++ Condition ++ ";",
    Result = epgsql:squery(C, Q),
    epgsql:close(C),
    Result.

findByAttributes(Table, Condition) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Q = "select * from " ++
	  Table ++ " where " ++ Condition ++ ";",
    {ok, Col, Row} = epgsql:squery(C, Q),
    Res = [maps:from_list(lists:zipwith(fun (#column{name =
							 N},
					     V) ->
						{N, V}
					end,
					Col, tuple_to_list(R)))
	   || R <- Row],
    epgsql:close(C),
    Res.

elementIsInteger(Elem) ->
    (Elem == <<"id">>) or (Elem == <<"income">>).

findAll(Table) ->
    {ok, Col, Row} = selectAllFrom(Table),
    Res = [maps:from_list(lists:zipwith(fun (#column{name =
							 N},
					     V) ->
						IsNInteger =
						    elementIsInteger(N),
						if IsNInteger ->
						       {N,
							list_to_integer(binary_to_list(V))};
						   true -> {N, V}
						end
					end,
					Col, tuple_to_list(R)))
	   || R <- Row],
    Res.

selectAllFrom(Table) ->
    {ok, C} = epgsql:connect("localhost", "postgres",
			     "postgres",
			     #{database => "aiscocomplete", timeout => 4000}),
    Result = epgsql:squery(C, "select * from " ++ Table),
    epgsql:close(C),
    Result.
