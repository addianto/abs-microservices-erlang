-module(class_MStoryBoardModel_StoryBoardImpl).
-include_lib("../include/abs_types.hrl").
-behaviour(object).
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"StoryBoard">>, <<"Object">> ].

exported() -> #{  }.

%% --- Internal state and low-level accessors

-record(state,{'class'=class_MStoryBoardModel_StoryBoardImpl,'id'=null,'programName'=null,'title'=null,'content'=null,'publishDate'=null,'thumbnailUrl'=null}).
'init_internal'()->
    #state{}.

 %% model/StoryBoard.abs:20
'get_val_internal'(#state{'id'=G},'id')->
    G;
 %% model/StoryBoard.abs:21
'get_val_internal'(#state{'programName'=G},'programName')->
    G;
 %% model/StoryBoard.abs:22
'get_val_internal'(#state{'title'=G},'title')->
    G;
 %% model/StoryBoard.abs:23
'get_val_internal'(#state{'content'=G},'content')->
    G;
 %% model/StoryBoard.abs:24
'get_val_internal'(#state{'publishDate'=G},'publishDate')->
    G;
 %% model/StoryBoard.abs:25
'get_val_internal'(#state{'thumbnailUrl'=G},'thumbnailUrl')->
    G;
'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

 %% model/StoryBoard.abs:20
'set_val_internal'(S,'id',V)->
    S#state{'id'=V};
 %% model/StoryBoard.abs:21
'set_val_internal'(S,'programName',V)->
    S#state{'programName'=V};
 %% model/StoryBoard.abs:22
'set_val_internal'(S,'title',V)->
    S#state{'title'=V};
 %% model/StoryBoard.abs:23
'set_val_internal'(S,'content',V)->
    S#state{'content'=V};
 %% model/StoryBoard.abs:24
'set_val_internal'(S,'publishDate',V)->
    S#state{'publishDate'=V};
 %% model/StoryBoard.abs:25
'set_val_internal'(S,'thumbnailUrl',V)->
    S#state{'thumbnailUrl'=V}.

'get_state_for_modelapi'(S)->
    [
        { 'id', S#state.'id' }
        , { 'programName', S#state.'programName' }
        , { 'title', S#state.'title' }
        , { 'content', S#state.'content' }
        , { 'publishDate', S#state.'publishDate' }
        , { 'thumbnailUrl', S#state.'thumbnailUrl' }
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
     %% model/StoryBoard.abs:20--20
    put(this, C:set_val_internal(get(this),'id',0)),
     %% model/StoryBoard.abs:21--21
    put(this, C:set_val_internal(get(this),'programName',<<""/utf8>>)),
     %% model/StoryBoard.abs:22--22
    put(this, C:set_val_internal(get(this),'title',<<""/utf8>>)),
     %% model/StoryBoard.abs:23--23
    put(this, C:set_val_internal(get(this),'content',<<""/utf8>>)),
     %% model/StoryBoard.abs:24--24
    put(this, C:set_val_internal(get(this),'publishDate',<<""/utf8>>)),
     %% model/StoryBoard.abs:25--25
    put(this, C:set_val_internal(get(this),'thumbnailUrl',<<""/utf8>>)),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% model/StoryBoard.abs:27
 %% model/StoryBoard.abs:27
'm_getId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/StoryBoard.abs:27--27
        C:get_val_internal(get(this), 'id')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/StoryBoard.abs:28
 %% model/StoryBoard.abs:28
'm_setId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'id' => V_id_0 }),
    try
         %% model/StoryBoard.abs:28--28
        put(this, C:set_val_internal(get(this), 'id',maps:get('id', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/StoryBoard.abs:29
 %% model/StoryBoard.abs:29
'm_getProgramName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/StoryBoard.abs:29--29
        C:get_val_internal(get(this), 'programName')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getProgramName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/StoryBoard.abs:30
 %% model/StoryBoard.abs:30
'm_setProgramName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_programName_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'programName' => V_programName_0 }),
    try
         %% model/StoryBoard.abs:30--30
        put(this, C:set_val_internal(get(this), 'programName',maps:get('programName', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setProgramName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/StoryBoard.abs:31
 %% model/StoryBoard.abs:31
'm_getTitle'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/StoryBoard.abs:31--31
        C:get_val_internal(get(this), 'title')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getTitle and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/StoryBoard.abs:32
 %% model/StoryBoard.abs:32
'm_setTitle'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_title_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'title' => V_title_0 }),
    try
         %% model/StoryBoard.abs:32--32
        put(this, C:set_val_internal(get(this), 'title',maps:get('title', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setTitle and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/StoryBoard.abs:33
 %% model/StoryBoard.abs:33
'm_getContent'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/StoryBoard.abs:33--33
        C:get_val_internal(get(this), 'content')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getContent and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/StoryBoard.abs:34
 %% model/StoryBoard.abs:34
'm_setContent'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_content_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'content' => V_content_0 }),
    try
         %% model/StoryBoard.abs:34--34
        put(this, C:set_val_internal(get(this), 'content',maps:get('content', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setContent and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/StoryBoard.abs:35
 %% model/StoryBoard.abs:35
'm_getPublishDate'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/StoryBoard.abs:35--35
        C:get_val_internal(get(this), 'publishDate')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getPublishDate and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/StoryBoard.abs:36
 %% model/StoryBoard.abs:36
'm_setPublishDate'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_publishDate_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'publishDate' => V_publishDate_0 }),
    try
         %% model/StoryBoard.abs:36--36
        put(this, C:set_val_internal(get(this), 'publishDate',maps:get('publishDate', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setPublishDate and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/StoryBoard.abs:37
 %% model/StoryBoard.abs:37
'm_getThumbnailUrl'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/StoryBoard.abs:37--37
        C:get_val_internal(get(this), 'thumbnailUrl')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getThumbnailUrl and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/StoryBoard.abs:38
 %% model/StoryBoard.abs:38
'm_setThumbnailUrl'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_thumbnailUrl_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'thumbnailUrl' => V_thumbnailUrl_0 }),
    try
         %% model/StoryBoard.abs:38--38
        put(this, C:set_val_internal(get(this), 'thumbnailUrl',maps:get('thumbnailUrl', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setThumbnailUrl and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
