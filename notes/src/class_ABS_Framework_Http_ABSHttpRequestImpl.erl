-module(class_ABS_Framework_Http_ABSHttpRequestImpl).
-include_lib("../include/abs_types.hrl").
-behaviour(object).
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"ABSHttpRequest">>, <<"Object">> ].

exported() -> #{  }.

%% --- Internal state and low-level accessors

-record(state,{'class'=class_ABS_Framework_Http_ABSHttpRequestImpl,'requestInput'=null,'requestProperty'=null}).
'init_internal'()->
    #state{}.

 %% framework/ABSHttpRequest.abs:10
'get_val_internal'(#state{'requestInput'=G},'requestInput')->
    G;
 %% framework/ABSHttpRequest.abs:10
'get_val_internal'(#state{'requestProperty'=G},'requestProperty')->
    G;
'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

 %% framework/ABSHttpRequest.abs:10
'set_val_internal'(S,'requestInput',V)->
    S#state{'requestInput'=V};
 %% framework/ABSHttpRequest.abs:10
'set_val_internal'(S,'requestProperty',V)->
    S#state{'requestProperty'=V}.

'get_state_for_modelapi'(S)->
    [
        { 'requestInput', S#state.'requestInput' }
        , { 'requestProperty', S#state.'requestProperty' }
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[P_requestInput,P_requestProperty,Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
    put(this, C:set_val_internal(get(this),'requestInput',P_requestInput)),
    put(this, C:set_val_internal(get(this),'requestProperty',P_requestProperty)),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% framework/ABSHttpRequest.abs:11
 %% framework/ABSHttpRequest.abs:11
'm_getInput'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_key_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'key' => V_key_0 }),
    try
         %% framework/ABSHttpRequest.abs:12--12
        put(vars, (get(vars))#{'value' => m_ABS_StdLib_funs:f_fromJust(Cog,m_ABS_StdLib_funs:f_lookup(Cog,C:get_val_internal(get(this), 'requestInput'),maps:get('key', get(vars)),[O,DC| Stack]),[O,DC| Stack])}),
         %% framework/ABSHttpRequest.abs:13--13
        maps:get('value', get(vars))
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getInput and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% framework/ABSHttpRequest.abs:16
 %% framework/ABSHttpRequest.abs:16
'm_getRequestProperty'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_key_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'key' => V_key_0 }),
    try
         %% framework/ABSHttpRequest.abs:17--17
        put(vars, (get(vars))#{'value' => m_ABS_StdLib_funs:f_fromJust(Cog,m_ABS_StdLib_funs:f_lookup(Cog,C:get_val_internal(get(this), 'requestProperty'),maps:get('key', get(vars)),[O,DC| Stack]),[O,DC| Stack])}),
         %% framework/ABSHttpRequest.abs:18--18
        maps:get('value', get(vars))
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getRequestProperty and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% framework/ABSHttpRequest.abs:21
 %% framework/ABSHttpRequest.abs:21
'm_hasKey'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_key_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'key' => V_key_0 }),
    try
         %% framework/ABSHttpRequest.abs:22--22
        put(vars, (get(vars))#{'maybeKey' => m_ABS_StdLib_funs:f_lookup(Cog,C:get_val_internal(get(this), 'requestInput'),maps:get('key', get(vars)),[O,DC| Stack])}),
         %% framework/ABSHttpRequest.abs:23--23
        put(vars, (get(vars))#{'hasKey' => m_ABS_StdLib_funs:f_isJust(Cog,maps:get('maybeKey', get(vars)),[O,DC| Stack])}),
         %% framework/ABSHttpRequest.abs:24--24
        maps:get('hasKey', get(vars))
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method hasKey and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
