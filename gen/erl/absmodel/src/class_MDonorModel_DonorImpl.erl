-module(class_MDonorModel_DonorImpl).
-include_lib("../include/abs_types.hrl").
-behaviour(object).
-export([get_val_internal/2,set_val_internal/3,init_internal/0,get_state_for_modelapi/1,implemented_interfaces/0,exported/0]).
-compile(export_all).

implemented_interfaces() -> [ <<"Donor">>, <<"Object">> ].

exported() -> #{  }.

%% --- Internal state and low-level accessors

-record(state,{'class'=class_MDonorModel_DonorImpl,'id'=null,'name'=null,'email'=null,'phone'=null,'address'=null}).
'init_internal'()->
    #state{}.

 %% model/Donor.abs:18
'get_val_internal'(#state{'id'=G},'id')->
    G;
 %% model/Donor.abs:19
'get_val_internal'(#state{'name'=G},'name')->
    G;
 %% model/Donor.abs:20
'get_val_internal'(#state{'email'=G},'email')->
    G;
 %% model/Donor.abs:21
'get_val_internal'(#state{'phone'=G},'phone')->
    G;
 %% model/Donor.abs:22
'get_val_internal'(#state{'address'=G},'address')->
    G;
'get_val_internal'(_,_)->
    %% Invalid return value; handled by HTTP API when querying for non-existant field.
    %% Will never occur in generated code.
    none.

 %% model/Donor.abs:18
'set_val_internal'(S,'id',V)->
    S#state{'id'=V};
 %% model/Donor.abs:19
'set_val_internal'(S,'name',V)->
    S#state{'name'=V};
 %% model/Donor.abs:20
'set_val_internal'(S,'email',V)->
    S#state{'email'=V};
 %% model/Donor.abs:21
'set_val_internal'(S,'phone',V)->
    S#state{'phone'=V};
 %% model/Donor.abs:22
'set_val_internal'(S,'address',V)->
    S#state{'address'=V}.

'get_state_for_modelapi'(S)->
    [
        { 'id', S#state.'id' }
        , { 'name', S#state.'name' }
        , { 'email', S#state.'email' }
        , { 'phone', S#state.'phone' }
        , { 'address', S#state.'address' }
    ].
%% --- Constructor: field initializers and init block

'init'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},[Stack])->
    C=(get(this))#state.class,
    put(vars, #{}),
     %% model/Donor.abs:18--18
    put(this, C:set_val_internal(get(this),'id',0)),
     %% model/Donor.abs:19--19
    put(this, C:set_val_internal(get(this),'name',<<""/utf8>>)),
     %% model/Donor.abs:20--20
    put(this, C:set_val_internal(get(this),'email',<<""/utf8>>)),
     %% model/Donor.abs:21--21
    put(this, C:set_val_internal(get(this),'phone',<<""/utf8>>)),
     %% model/Donor.abs:22--22
    put(this, C:set_val_internal(get(this),'address',<<""/utf8>>)),
    O.
%% --- Class has no recovery block

%% --- Methods

 %% model/Donor.abs:24
 %% model/Donor.abs:24
'm_getId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Donor.abs:24--24
        C:get_val_internal(get(this), 'id')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Donor.abs:25
 %% model/Donor.abs:25
'm_setId'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_id_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'id' => V_id_0 }),
    try
         %% model/Donor.abs:25--25
        put(this, C:set_val_internal(get(this), 'id',maps:get('id', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setId and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Donor.abs:26
 %% model/Donor.abs:26
'm_getName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Donor.abs:26--26
        C:get_val_internal(get(this), 'name')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Donor.abs:27
 %% model/Donor.abs:27
'm_setName'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_name_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'name' => V_name_0 }),
    try
         %% model/Donor.abs:27--27
        put(this, C:set_val_internal(get(this), 'name',maps:get('name', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setName and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Donor.abs:28
 %% model/Donor.abs:28
'm_getEmail'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Donor.abs:28--28
        C:get_val_internal(get(this), 'email')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getEmail and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Donor.abs:29
 %% model/Donor.abs:29
'm_setEmail'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_email_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'email' => V_email_0 }),
    try
         %% model/Donor.abs:29--29
        put(this, C:set_val_internal(get(this), 'email',maps:get('email', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setEmail and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Donor.abs:30
 %% model/Donor.abs:30
'm_getPhone'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Donor.abs:30--30
        C:get_val_internal(get(this), 'phone')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getPhone and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Donor.abs:31
 %% model/Donor.abs:31
'm_setPhone'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_phone_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'phone' => V_phone_0 }),
    try
         %% model/Donor.abs:31--31
        put(this, C:set_val_internal(get(this), 'phone',maps:get('phone', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setPhone and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Donor.abs:32
 %% model/Donor.abs:32
'm_getAddress'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O }),
    try
         %% model/Donor.abs:32--32
        C:get_val_internal(get(this), 'address')
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method getAddress and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
 %% model/Donor.abs:33
 %% model/Donor.abs:33
'm_setAddress'(O=#object{oid=Oid,cog=Cog=#cog{ref=CogRef,dcobj=DC}},V_address_0,Stack)->
    C=(get(this))#state.class,
    put(vars, #{ 'this' => O,
 'address' => V_address_0 }),
    try
         %% model/Donor.abs:33--33
        put(this, C:set_val_internal(get(this), 'address',maps:get('address', get(vars)))),
        dataUnit
        
    catch
        _:Exception ->
            io:format(standard_error, "Uncaught ~s in method setAddress and no recovery block in class definition, killing object ~s~n", [builtin:toString(Cog, Exception), builtin:toString(Cog, O)]),
            io:format(standard_error, "stacktrace: ~tp~n", [erlang:get_stacktrace()]),
            object:die(O, Exception), exit(Exception)
    end.
